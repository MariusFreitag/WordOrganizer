﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Resources;

namespace WordOrganizer
{
    public partial class MainWindow : Window
    {
        private Config cfg;

        private bool usesDefaultTemplate = false;

        private bool UsesDefaultTemplate
        {
            get
            {
                return usesDefaultTemplate;
            }
            set
            {
                usesDefaultTemplate = value;
                if (value)
                {
                    TxtTemplateDe.IsEnabled = false;
                    TxtTemplateEn.IsEnabled = false;
                }
                else
                {
                    TxtTemplateDe.IsEnabled = true;
                    TxtTemplateEn.IsEnabled = true;
                }
            }
        }

        // Used to focus other instances
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hwnd);

        public MainWindow()
        {
            InitializeComponent();

            #region Check for other instance and focus
            Process current = Process.GetCurrentProcess();
            foreach (Process process in Process.GetProcessesByName(current.ProcessName))
            {
                if (process.Id != current.Id)
                {
                    SetForegroundWindow(process.MainWindowHandle);
                    Environment.Exit(0);
                }
            }
            #endregion

            Initialize();
        }

        private void Initialize()
        {
            #region Read Configuration

            try
            {
                cfg = JsonConvert.DeserializeObject<Config>(File.ReadAllText("WordOrganizer.config"));
            }
            catch
            {
                cfg = null;
            }
            cfg = cfg ?? new Config();

            #endregion

            #region Apply Configuration

            UsesDefaultTemplate = cfg.UsesDefaultTemplate;
            TxtWordExecutablePath.Text = cfg.WordExecutablePath;
            TxtRootFolder.Text = cfg.RootFolder;
            ComboBoxCategory.Text = cfg.Category;
            TxtTemplateDe.Text = cfg.TemplateDe;
            TxtTemplateEn.Text = cfg.TemplateEn;

            TxtDate.Text = DateTime.Now.ToShortDateString();

            #endregion

            TxtTitle.Focus();

            TimeSetterLoop();
        }

        private void CreateDocument()
        {
            if (string.IsNullOrEmpty(TxtRootFolder.Text))
            {
                TxtRootFolder.Text = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }

            Dictionary<string, string> placeholders = new Dictionary<string, string>
            {
                {"[Title]", TxtTitle.Text.Replace("\"", "")},
                {"[Author]", "Marius Freitag" },
                {"[Category]", ComboBoxCategory.Text},
                {"[Date]", TxtDate.Text}
            };

            #region Decompress

            string zipFileTempPath = "";

            if (usesDefaultTemplate)
            {
                StreamResourceInfo sri = Application.GetResourceStream(
                    new Uri(
                        string.Format(
                            "/WordOrganizer;component/Template_{0}.docx",
                            ChkEnglishTemplate.IsChecked == true ? "EN" : "DE"
                            ),
                        UriKind.Relative));

                zipFileTempPath = Path.Combine(Path.GetTempPath(), "WordOrganizerTemp", DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".tmp");
                if (Directory.Exists(Path.GetDirectoryName(zipFileTempPath)))
                {
                    Directory.Delete(Path.GetDirectoryName(zipFileTempPath), true);
                }
                Directory.CreateDirectory(Path.GetDirectoryName(zipFileTempPath));

                using (FileStream outputStream = new FileStream(zipFileTempPath, FileMode.Create, FileAccess.Write))
                {
                    int cnt = 0;
                    const int len = 4096;
                    byte[] buffer = new byte[len];

                    while ((cnt = sri.Stream.Read(buffer, 0, len)) != 0)
                    {
                        outputStream.Write(buffer, 0, cnt);
                    }
                }
            }
            else
            {
                zipFileTempPath = ChkEnglishTemplate.IsChecked == true ? TxtTemplateEn.Text : TxtTemplateDe.Text;
            }

            string destZipDirTempPath = "";

            try
            {
                destZipDirTempPath = Path.Combine(Path.GetTempPath(), "WordOrganizerTemp", DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss"));
                ZipFile.ExtractToDirectory(zipFileTempPath, destZipDirTempPath, null);

                if (usesDefaultTemplate)
                {
                    File.Delete(zipFileTempPath);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Could not open the template!");
                this.Show();
                return;
            }

            #endregion

            #region Replace Placeholders

            string[] docPaths = { @"/word/header1.xml", @"/word/header2.xml", @"/word/header3.xml", @"/word/footer1.xml", @"/word/footer2.xml", @"/word/footer3.xml", @"/word/document.xml" };


            foreach (var doc in docPaths)
            {
                string filePath = destZipDirTempPath + doc;
                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Close();
                }

                string content = File.ReadAllText(filePath);
                foreach (var placeholder in placeholders)
                {
                    content = content.Replace(placeholder.Key, placeholder.Value);
                }

                File.WriteAllText(filePath, content);
            }
            #endregion

            #region Compress

            string docPath = Path.Combine(TxtRootFolder.Text, ReplaceIllegalPathCharacters(ComboBoxCategory.Text), ReplaceIllegalPathCharacters(TxtTitle.Text) + ".docx");

            if (!Directory.Exists(Path.GetDirectoryName(docPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(docPath));
            }

            if (File.Exists(docPath))
            {
                MessageBox.Show(String.Format("File {0} already exists!", Path.GetFileName(docPath)));
                Directory.Delete(destZipDirTempPath, true);
                return;
            }
            else
            {
                ZipFile.CreateFromDirectory(destZipDirTempPath, docPath);
                OpenFile(docPath);
            }

            Directory.Delete(Path.GetDirectoryName(destZipDirTempPath), true);
            #endregion
        }

        private static string ReplaceIllegalPathCharacters(string input)
        {
            return input.Replace("\\", "")
                .Replace("/", "-")
                .Replace("\"", "")
                .Replace(":", "-")
                .Replace("/", "_")
                .Replace("\\", "_")
                .Replace(":", "_")
                .Replace("*", "_")
                .Replace("?", "_")
                .Replace("\"", "_")
                .Replace("<", "_")
                .Replace(">", "_")
                .Replace("|", "_");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateDocument();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cfg.WordExecutablePath = TxtWordExecutablePath.Text;
            cfg.RootFolder = TxtRootFolder.Text;
            cfg.Category = ComboBoxCategory.Text;
            cfg.UsesDefaultTemplate = UsesDefaultTemplate;
            cfg.TemplateDe = TxtTemplateDe.Text;
            cfg.TemplateEn = TxtTemplateEn.Text;

            if (!File.Exists("WordOrganizer.config") || File.ReadAllText("WordOrganizer.config") != JsonConvert.SerializeObject(cfg, Formatting.Indented))
                File.WriteAllText("WordOrganizer.config", JsonConvert.SerializeObject(cfg, Formatting.Indented));
        }

        private void OpenFile(string path)
        {
            try
            {
                Process.Start(TxtWordExecutablePath.Text, $"\"{path}\"");
                TxtTitle.Text = "";
                this.WindowState = WindowState.Minimized;
            }
            catch (Exception)
            {
                MessageBox.Show("Could not open the file!" + Environment.NewLine + path);
            }
        }

        private void LblSubject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start(Path.Combine(TxtRootFolder.Text, ComboBoxCategory.Text));
        }

        private void PathTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog openfile = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = openfile.ShowDialog();
            if (openfile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ((TextBox)sender).Text = openfile.SelectedPath;
            }
        }

        private void TimeSetterLoop()
        {
            Thread t = new Thread((ThreadStart)delegate
            {
                while (true)
                {
                    this.Dispatcher.Invoke(delegate
                    {
                        LblTime.Content = DateTime.Now.ToString("dd.MM.yyyy - HH:mm:ss");
                    });
                    Thread.Sleep(1000);
                }
            });
            t.IsBackground = true;
            t.Start();
        }

        private void LblTemplates_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UsesDefaultTemplate = !UsesDefaultTemplate;
        }
    }
}