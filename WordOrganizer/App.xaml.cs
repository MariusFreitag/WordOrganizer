﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace WordOrganizer
{
    public partial class App : Application
    {
        protected void App_Startup(object sender, StartupEventArgs e)
        {
            new MainWindow().Show();
        }
    }
}
