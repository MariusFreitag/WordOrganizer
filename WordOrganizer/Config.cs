﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordOrganizer
{
    class Config
    {
        public string WordExecutablePath { get; set; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Microsoft Office", "root", "Office16", "WINWORD.EXE");
        public string RootFolder { get; set; } = "";
        public string Category { get; set; } = "";
        public bool UsesDefaultTemplate { get; set; } = true;
        public string TemplateDe { get; set; } = "";
        public string TemplateEn { get; set; } = "";
    }
}
